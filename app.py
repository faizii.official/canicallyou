import speech_recognition as sr

# Initialize the recognizer
r = sr.Recognizer()

# Use the default microphone as the audio source
with sr.Microphone() as source:
    print("Listening...")

    # Adjust for ambient noise levels
    r.adjust_for_ambient_noise(source)

    # Continuous audio listening loop
    while True:
        # Listen for audio input
        audio = r.listen(source)

        try:
            # Convert speech to text
            text = r.recognize_google(audio)

            # Print the recognized text
            print("Recognized:", text)

        except sr.UnknownValueError:
            # If speech is unintelligible or not recognized
            print("Could not understand audio.")

        except sr.RequestError as e:
            # If there's an issue with the speech recognition service
            print("Could not request results from the speech recognition service:", str(e))
