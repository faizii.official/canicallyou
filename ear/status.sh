#!/bin/bash
 amixer sset Capture 33% 
while true
do
  noise=$(rec -n stat trim 1 0.1 2>&1 | awk '/^Maximum amplitude/' | cut -b 24-99)

  if (( $(echo "$noise > 0.3" | bc -l) ))
  then
    clear
    echo "speaking"
    touch speaking
    rm silence
    echo "$noise"
  else
    clear
    echo "silence"
    touch silence
    rm speaking 

    echo "$noise"
  fi
done
