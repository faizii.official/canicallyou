
# Convert audio to text using SpeechRecognition
if [ -f "recording0.wav" ]; then

touch conv0
output=$(python - <<END
import speech_recognition as sr

r = sr.Recognizer()

with sr.AudioFile("recording0.wav") as source:
    audio = r.record(source)

try:
    text = r.recognize_google(audio)
    print(text)
    exit_code = 0  # Set the exit code to indicate success
except sr.UnknownValueError:
    print("Speech recognition could not understand audio")
    exit_code = 1  # Set the exit code to indicate failure
except sr.RequestError:
    print("Could not request results from Speech Recognition service")
    exit_code = 1  # Set the exit code to indicate failure

exit(exit_code)  # Exit with the appropriate exit code
END
)

# Check the exit status of the Python script
if [ $? -eq 0 ]; then
    echo "Success"
else
    echo "Fail"
fi

# Save the output to a file

echo "$output" >> "output.txt"

# Print the output to the console
echo "$output"
rm conv0 recording0.wav
else 
echo "waiting for input"
fi