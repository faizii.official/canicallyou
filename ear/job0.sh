#!/bin/bash

lock0=true
# Set the duration for recording in seconds
while $lock0
do
if [ -f "recording0.wav" ]; then
  lock0=true
  else
  lock0=false
fi
  
if [ -f "recording0.wav" ]; then
  echo "lock exist waiting.."
  
else
  echo "File does not exist."
  touch rec0
  duration=8

# Specify the output file name
output_file="recording0.wav"

# Record audio for the specified duration
arecord -d $duration -f cd -t wav $output_file
bash ear/convert0.sh &

rm rec0
bash ear/job1.sh
fi
done