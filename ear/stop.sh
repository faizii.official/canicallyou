kill $(pgrep -f "bash ear/status.sh")
rm *.txt speaking silence *.mp3 *.wav rec* conv*
pwd
ls
pids=$(pidof bash)

#!/bin/bash

# Specify the name of the script or process to terminate
process_name="job0.sh"

# Retrieve the PIDs of running processes matching the specified name
pids=$(pgrep -f "$process_name")

# Loop through each PID and terminate the corresponding process
for pid in $pids; do
  echo "Terminating PID: $pid"
  kill $pid
done

process_name="job1.sh"

# Retrieve the PIDs of running processes matching the specified name
pids=$(pgrep -f "$process_name")

# Loop through each PID and terminate the corresponding process
for pid in $pids; do
  echo "Terminating PID: $pid"
  kill $pid
done
