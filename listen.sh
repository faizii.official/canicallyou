#!/bin/bash
# Set up the variables
INPUT_FILE="audio.wav"
OUTPUT_FILE="output.txt"
start_recording() {
  echo "Recording started..."
rec -r 16000 -c 1 "audio.wav" silence 1 0.1 3% 5 1.5 20% #33% mic
}

# Function to stop recording
stop_recording() {
  echo "Recording stopped."
  pkill -INT rec
}

init() {
    start_recording
    stop_recording
    convert

}


convert() {

# Convert audio to text using SpeechRecognition
output=$(python - <<END
import speech_recognition as sr

r = sr.Recognizer()

with sr.AudioFile("audio.wav") as source:
    audio = r.record(source)

try:
    text = r.recognize_google(audio)
    print(text)
    exit_code = 0  # Set the exit code to indicate success
except sr.UnknownValueError:
    print("Speech recognition could not understand audio")
    exit_code = 1  # Set the exit code to indicate failure
except sr.RequestError:
    print("Could not request results from Speech Recognition service")
    exit_code = 1  # Set the exit code to indicate failure

exit(exit_code)  # Exit with the appropriate exit code
END
)

# Check the exit status of the Python script
if [ $? -eq 0 ]; then
    echo "Success"
else
    echo "Fail"
fi

# Save the output to a file

echo "$output" > "output.txt"

# Print the output to the console
echo "$output"

}

init