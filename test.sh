#!/bin/bash

speak() {
    aws polly synthesize-speech --output-format mp3 --text "$voice" --voice-id Matthew output.mp3
    mpg123 output.mp3
    rm -f output.mp3
}

listen() {
    # Set up the variables
bash listen.sh

    response=$(cat output.txt)
    if [ "$response" = "Speech recognition could not understand audio" ]; then
        voice="Sorry! There is a lot of disturbance here. Please say that again!"
        speak
        listen
    else
        echo "Heard: $response"
    fi
}

# Clean up previous output files
rm -f output.mp3
rm -f output.txt

# Prompt the user
voice="Hey, hope you are doing well! I am a Bot calling from Deqode Pvt Ltd Pune. I have gone through your CV from naukri.com. Are you looking for a job change? Please say 'yes' or 'no' after the beep."
speak

# Run the speech recognition script
listen

# Process the user's response
if [ "$response" = "yes" ]; then
    voice="Hmm, okay. Do you have some time so we can talk?"
else
    voice="Hmm, alright. Goodbye! Thanks for your precious time."
fi
speak

# Additional questions
if [ "$response" = "yes" ]; then
    voice="Great! Can you please tell me about your current job role and responsibilities?"
    speak
    listen
    # Process the user's response and perform actions accordingly
    
    voice="That sounds interesting. What motivates you to seek a job change?"
    speak
    listen
    # Process the user's response and perform actions accordingly
    
    voice="What are your key skills or areas of expertise?"
    speak
    listen
    # Process the user's response and perform actions accordingly
    
    voice="Have you worked on any projects or initiatives that you are proud of? If yes, please share."
    speak
    listen
    # Process the user's response and perform actions accordingly
    
    voice="Do you have any specific industries or companies you are targeting for your next job?"
    speak
    listen
    # Process the user's response and perform actions accordingly
    
    voice="What are your salary expectations for the new role?"
    speak
    listen
    # Process the user's response and perform actions accordingly
    
    voice="Do you have any questions or concerns you would like to discuss with me?"
    speak
    listen
    # Process the user's response and perform actions accordingly
    
    voice="Thank you for your time. We will get back to you soon. Have a great day!"
    speak
fi
